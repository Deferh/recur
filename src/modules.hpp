int  product(int a, int b)
{
	//complete me (recursively)!
	int result = 0;
	
	if(a == 0 || b == 0)
	{
		return 0;
	}
	
	if(a > 0 and b > 0)
	{
		result += b;
		return (a + product(a, b - 1));
	}
				
	if(a < 0 and b > 0)
	{
		result += b;
		return -product(-a, b);
	}
	
	if(a > 0 and b < 0)
	{
		result += b;
		return -product(a, -b);
	}
	
	if(a < 0 and b < 0)
	{
		result += b;
		return product(-a, -b);
	}
	
	return result;
}
